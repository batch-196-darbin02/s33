// express package was imported as express
const express = require("express");

// invoked express package to create a server/api and saved it in variable which we can refer to later to create routes.
const app = express();

// express.json() is a method that allows us to handle stream of data from our client and receive the data and automatically parce the incoming JSON from the request
// app.use() is a method usedd to run another function or method for our expressJS API. It is used to run middlewares (functions that add features to our application)
app.use(express.json());

// variable for port assignment
const port = 4000;

// mock collection of courses:
let courses = [
	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
];

let users = [
	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubuTofu"
	}
]

// creating a route in Express:
// access express to have access to its route methods.
/*
	Syntax:
	app.method('endpoint',(request,response) => {
		
		// send is a method similar to end() that it sends the data/message and ends the response.
		// it also automatically creates and adds the headers.
		response.send()
	})
*/
app.get('/',(request,response) => {

	response.send("Hello from our first ExpressJS GET method route.");

});

app.post('/',(request,response) => {

	response.send("Hello from our first ExpressJS POST method route.")
});

app.put('/',(request,response) => {

	response.send("Hello from our first ExpressJS PUT method route.")
});

app.delete('/',(request,response) => {

	response.send("Hello from our first ExpressJS DELETE method route.")
});

app.get('/courses',(request,response) => {

	response.send(courses);
})

// a route to be able to add a new course from an input from a request

app.post('/courses',(request,response) => {

	// request or request.body contains the body of the request or the input passed
	// console.log(request.body); // will result to an object
	// with express.json() the data streat has been captured, the data input has been pased into a JS Object.
	// Note: Every time you need to access or use request body, log it in the console first.

	// add the input as request.body into our courses array
	courses.push(request.body);

	// console.log(courses);

	response.send(courses);

})

app.get('/users',(request,response) => {

	response.send(users);
})

app.post('/users',(request,response) => {

	users.push(request.body);

	response.send(users);
})

app.delete('/users',(request,response) => {

	users.pop(request.body);

	response.send("The last user in your array has been successfully deleted.");
})


// used to listen() method of express to assign a port to our server and send a message.
app.listen(port,() => console.log(`Express API running at port 4000.`));